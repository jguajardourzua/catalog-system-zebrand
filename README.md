**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

# Catalogo de productos

El proyecto corresponde a una API GraphQL que permite creear , modificar y eliminar productos y usuarios, guardar visualizaciones de los productos y notificar por email cuando se agrega un nuevo producto.

## Comenzando 🚀

El proyecto esta desarrollado con Phyton , utilizando el Framework Flask, incorpora autentificación con JWT. Se utiliza docker para correr el proyecto junto a una bbdd mySql.

 

### Pre-requisitos 📋

Para instalaar el proyecto es necesario tener previamente instalado Docker.


### Instalación 🔧

Se debe ubicar en la raiz del proyecto y correr el siguiente comando

```
docker-compose up
```

Para visualizar el view de GraphQL se debe ingresar a la siguiente ruta : 

```
http://localhost:5000/graphql
```

Más información sobere el uso de la API se encuentra en 

```
http://localhost:5000/doc
```

## Construido con 🛠️

Las Herramientas utilizadas para el desarrollo son 


* Python con Flask
* SqlAlchemy como ORM
* Graphene
* MySql
* JWT
* Docker y Docker compose

## Ejemplo con 🛠️

Para comenzar a utilizar el sistema, primero se debe agregar un Usuario de la siguiente manera

```
mutation {
  addUser(address:"",age:0,email:"",name:"",password:""){
    user
  }
}

```

Posteriormente, se debe autenticar con dicho usuario para obtener el token.

Para autenticar, el username corresponde al email de usuario.
```
mutation{
  	authUser(password:"", username:""){
    accessToken
  }
}
```

si la autentificación ha sido correcta, se deberia obtener el siguiente resultado

```
{
  "data": {
    "authUser": {
      "accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoiYWNjZXNzIiwiaWF0IjoxNjE4MjI2NjMwLCJuYmYiOjE2MTgyMjY2MzAsImp0aSI6IjUwNDA0OTAwLWM3NjctNGRjNC1hZDVhLTAyMGY2ZmZhYzM3YSIsImlkZW50aXR5IjoiZ3VhamFyZG9AZ3VhamFyZG8uY29tIiwiZXhwIjoxNjE4MjI3NTMwfQ.LkZvnQGer4dcxiwdwTI6tlyKK81CKOjALGhOEnOYRV8"
    }
  }
}
```

Luego , este token es necesario para utilizar el resto de las llamnadas a la API.
 


