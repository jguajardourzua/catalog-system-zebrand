from config import Config

from flask import Flask
from flask_graphql import GraphQLView
from flask_sqlalchemy import SQLAlchemy
import graphdoc
from flask_graphql_auth import (
    AuthInfoField,
    GraphQLAuth,
    get_jwt_identity,
    get_raw_jwt,
    create_access_token,
    create_refresh_token,
    query_jwt_required,
    mutation_jwt_refresh_token_required,
    mutation_jwt_required,
)

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)
    
    # Obtener la configuracion del ambiente elegido
    app.config.from_object(get_environment_config())
    auth=GraphQLAuth(app)



    # Inicializa la conexion a la BD
    db.init_app(app)

    from app.schema import schema
    app.add_url_rule(
        '/graphql',
        view_func=GraphQLView.as_view(
            'graphql',
            schema=schema,
            graphiql=True
        )
    )

    @app.before_first_request
    def initialize_database():
        """ Crea las tablas de la BD """
        db.create_all()

    @ app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/")
    def test():
        return "Test ok!"

    @app.route("/doc")
    def docs():
        html = graphdoc.to_doc(schema)
        return html
    
    return app

def get_environment_config():
    if Config.ENV == "TESTING":
        return "config.TestingConfig"
    elif Config.ENV == "DEVELOPMENT":
        return "config.DevelopmentConfig"