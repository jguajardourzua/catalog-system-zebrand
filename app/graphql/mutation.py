import graphene

import bcrypt
from app import db, create_access_token, create_refresh_token,mutation_jwt_required
from app.graphql.object import  UserObject as UserObj, ProductObject as ProductObj, ProductViewObject as ProductViewObj
from app.model import User as UserModel, Product as ProductModel, ProductView as ProductViewModel
#from app.comun.send_email import SendNotification
import bcrypt
from  app.comun.email_send import SendNotification
 

class AuthMutation(graphene.Mutation):
    class Arguments(object):
        username = graphene.String()
        password = graphene.String()

    access_token = graphene.String()
    refresh_token = graphene.String()

    @classmethod
    def mutate(cls, _, info, username, password):
        user = db.session.query(UserModel).filter_by(email=username).first()
        ## validar el password obtenido con el password de usuario
       
        if not user:
            raise Exception('Authenication Failure : User is not registered')
        if bcrypt.checkpw(password.encode('utf-8'),user.password.encode('utf-8')):
            return AuthMutation(
                access_token=create_access_token(username),
                refresh_token=create_refresh_token(username),
            )
        else:
            raise Exception('Password is not valid')



class AddUser(graphene.Mutation):
    class Arguments:
        name        = graphene.String()
        address     = graphene.String()
        email       = graphene.String()
        age         = graphene.String()
        password    = graphene.String()
        
    
    user = graphene.Field(lambda : UserObj)


    def mutate(self, info, name, age, address, email,password):
        passhash = bcrypt.hashpw(password.encode('utf-8'),bcrypt.gensalt())
        new_user =  UserModel ( name=name , age=age, address=address, email=email, password=passhash)

        db.session.add(new_user)
        db.session.commit()

        return AddUser( user=new_user)

class AddProduct(graphene.Mutation):
    class Arguments:
        name        = graphene.String()
        description = graphene.String()
        category    = graphene.String()
        sku         = graphene.String()
        price       = graphene.Int()
        token       = graphene.String()

        
    
    #ok=graphene.Boolean()
    product = graphene.Field(lambda : ProductObj)
    
    @mutation_jwt_required
    def mutate(self, info, name, description, category, sku,price):
        new_product = ProductModel (name=name, description=description,category=category,sku=sku, price=price)
        db.session.add(new_product)
        db.session.commit()
        #send notification
        #sendEmail()
        SendNotification.sendEmail()
        return AddProduct(product=new_product)

class ModifyProduct(graphene.Mutation):
    class Arguments:
        uuid        = graphene.ID()
        name        = graphene.String()
        description = graphene.String()
        category    = graphene.String()
        sku         = graphene.String()
        price       = graphene.Int()
        token       = graphene.String()

    ok = graphene.Boolean()
    #product = graphene.Field()

    @mutation_jwt_required
    def mutate(self, info, uuid=None,name=None,description=None,category=None,sku=None,price=None):
        if not uuid:
            return ModifyProduct(ok=False)

        modify_product = db.session.query(ProductModel).filter_by(uuid=uuid).first();
        
        if name:
            modify_product.name=name
        if description:
            modify_product.description=description
        if price:
            modify_product.price = price
        if category:
            modify_product.category = category
        if sku:
            modify_product.sku = sku

        db.session.commit()
        return ModifyProduct(ok=True)

class ModifyUser(graphene.Mutation):
    class Arguments:
        uuid        = graphene.ID()
        name        = graphene.String()
        address     = graphene.String()
        email       = graphene.String()
        age         = graphene.String()
        token       = graphene.String()
    
    ok=graphene.Boolean()

    def mutate(self, info, uuid=None, name=None,address=None,email=None,age=None):
        if not uuid:
            return ModifyUser(ok=False)

        modify_user = db.session.query(UserModel).filter_by(uuid=uuid).first();
       
        if name:
            modify_user.name =name
        if address:
            modify_user.address = address
        if email:
            modify_user.email = email
        if age:
            modify_user.age = age
        db.session.commit()
        return ModifyUser(ok=True)


class DeleteProduct(graphene.Mutation):
    ok=graphene.Boolean()

    class Arguments:
        uuid  = graphene.ID()
        token = graphene.String()
    
    @mutation_jwt_required
    def mutate(self, info, uuid):
        product_delete = db.session.query(ProductModel).filter_by(uuid=uuid).first()
        db.session.delete(product_delete)
        ok=True
        db.session.commit()
        return DeleteProduct(ok=ok)

class DeleteUser(graphene.Mutation):
    class Arguments:
        uuid    = graphene.ID()
        token   = graphene.String()
    
    ok = graphene.Boolean()

    @mutation_jwt_required
    def mutate(self,info,uuid):
        user_delete = db.session.query(UserModel).filter_by(uuid=uuid).first();
        db.session.delete(user_delete)
        db.session.commit()
        return DeleteUser(ok=True)

class AddViewProduct(graphene.Mutation):
    class Arguments:
        idProduct   = graphene.Int()
        token       = graphene.String()

    ok = graphene.Boolean();

    @mutation_jwt_required
    def mutate(self, info, idProduct):
        product = db.session.query(ProductModel).filter_by(uuid=idProduct).first()
        if not product:
            raise Exception('The product does not exist ')

        productView = db.session.query(ProductViewModel).filter_by(idProduct=idProduct).first()
        if productView:
            productView.views = productView.views + 1;
        else:
            new_product_view =  ProductViewModel ( idProduct=idProduct, views = 1)
            db.session.add(new_product_view)
        db.session.commit()
        return AddViewProduct(ok=True)


class Mutation(graphene.ObjectType):
    auth_user           = AuthMutation.Field()
    add_user            = AddUser.Field()
    delete_user         = DeleteUser.Field()
    modify_user         = ModifyUser.Field()

    add_product         = AddProduct.Field()
    delete_product      = DeleteProduct.Field()
    modify_product      = ModifyProduct.Field()

    add_view_product    = AddViewProduct.Field()
   
