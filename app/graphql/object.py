import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.model import  User as UserModel, Product as ProductModel, ProductView as ProductViewModel
#from app.model import Product as ProductModel, UserAdmin as UserAdminModel, ModificationHistory as ModificationHistoryModel, ViewProduct as ViewProductModel, StockProduct as StockProductModel

 

class UserObject(SQLAlchemyObjectType):
    class Meta:
        model = UserModel
        interfaces = (graphene.relay.Node,)

class ProductObject(SQLAlchemyObjectType):
    class Meta:
        model = ProductModel
        interfaces = (graphene.relay.Node,) 
        
class ProductViewObject(SQLAlchemyObjectType):
    class Meta:
        model = ProductViewModel
        interfaces = (graphene.relay.Node,) 
