import graphene

from app import mutation_jwt_required
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyConnectionField
from app.graphql.object import UserObject as UserObj, ProductObject as ProductObj, ProductViewObject as ProductViewObj
from app.model import  User as UserModel, Product as ProductModel, ProductView as dProductViewModel 


class Query(graphene.ObjectType):
    node = relay.Node.Field()
    users = graphene.List(lambda: UserObj, token=graphene.String(), uuid=graphene.String() )
    products = graphene.List(lambda:ProductObj, token=graphene.String(), uuid=graphene.String() )
    views = graphene.List(lambda:ProductViewObj, idProduct=graphene.Int() )

    @mutation_jwt_required
    def resolve_users(self, info,  uuid=None):
        query = UserObj.get_query(info)
        if uuid:
            query = query.filter(UserModel.uuid == uuid )
        return query.all()
    
    @mutation_jwt_required
    def resolve_products(self, info , uuid=None):
        query = ProductObj.get_query(info)
        if uuid:
            query = query.filter(ProductModel.uuid==uuid)

        return query.all()

    def resolve_views(self, info, idProduct=None):
        query = ProductViewObj.get_query(info)
        if idProduct:
            query = query.filter(ProductViewModel.idProduct==idProduct)

        return query.all()
