from app import db
 


class User(db.Model):
    __tablename__ ="Users"
    uuid = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(100))
    email = db.Column(db.String(100))
    age = db.Column(db.String(256))
    name = db.Column(db.String(256))
    password = db.Column(db.String(100))
    

    def __repr__(self):
        return f"<User {self.name}>"

class Product(db.Model):
    __tablename__ ="Products"
    uuid        = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String(256))
    description = db.Column(db.String(256))
    category    = db.Column(db.String(100))
    sku         = db.Column(db.String(10))
    price       = db.Column(db.Integer)
    views       = db.relationship("ProductView")
    
    def __repr__(self):
        return f"<Product {self.uuid}>"

class ProductView(db.Model):
    __tablename__   = "ProductViews"
    id            = db.Column(db.Integer, primary_key=True)
    views           = db.Column(db.Integer)
    idProduct       = db.Column(db.Integer, db.ForeignKey("Products.uuid"))
    product         = db.relationship("Product")
    

    def __repr__(self):
        return f"<ProductViews {self.id}>"


