import os
import click


class Config(object):
   ENV = os.environ["ENV"] if "ENV" in os.environ else "DEVELOPMENT"
   CSRF_ENABLED = True
   SECRET_KEY = os.urandom(12) #"this_is_a_secret_key"
   JWT_SECRET_KEY=os.urandom(20) 
   SQLALCHEMY_TRACK_MODIFICATIONS = False
   REFRESH_EXP_LENGTH = 30
   ACCESS_EXP_LENGTH = 10


class DevelopmentConfig(Config):
   DEBUG = True
   SQLALCHEMY_DATABASE_URI = "mysql+pymysql://" + os.environ["DB_USERNAME"] + ":" \
                             + os.environ["DB_PASSWORD"] + "@" \
                             + os.environ["DB_HOST"] + ":" \
                             + os.environ["DB_PORT"] + "/" \
                             + os.environ["DB_DATABASE"]
   click.echo(SQLALCHEMY_DATABASE_URI)


class TestingConfig(Config):
   DEBUG = False
   SQLALCHEMY_DATABASE_URI = "mysql+pymysql://" + os.environ["DB_USERNAME"] + ":" \
                             + os.environ["DB_PASSWORD"] + "@" \
                             + os.environ["DB_HOST"] + ":" \
                             + os.environ["DB_PORT"] + "/" \
                             + os.environ["DB_DATABASE"]

